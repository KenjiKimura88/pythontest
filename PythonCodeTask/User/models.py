from __future__ import unicode_literals

from django.db import models
from datetime import date
import random

def random_string():
    return str(random.randint(1, 100))

class User(models.Model):
    Username = models.CharField(max_length=100)
    Birthday = models.DateField("Date", default=date.today) #Default Data Today
    Random = models.IntegerField(default = random_string)
