from django.conf.urls import url

from .views import (
    UserList,
    UserDetail,
    UserCreation,
    UserUpdate,
    UserDelete
)
import views

urlpatterns = [

    url(r'^$', UserList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)$', UserDetail.as_view(), name='detail'),
    url(r'^new$', UserCreation.as_view(), name='new'),
    url(r'^edit/(?P<pk>\d+)$', UserUpdate.as_view(), name='edit'),
    url(r'^delete/(?P<pk>\d+)$', UserDelete.as_view(), name='delete'),
    url(r'^export/csv/$', views.export_users_csv, name='export_users_csv'),

]