from django import template
from datetime import date

register = template.Library()

@register.simple_tag
def check_eligible(birthday):
	difference_in_years = date.today().year - birthday.year
	return "allowed" if difference_in_years > 13 else "blocked"

@register.simple_tag
def bizzfuzz(number):
	result = number
	if number % 3 == 0:
		result = "Bizz"
	if number % 5 == 0:
		result = "Fuzz"
	if number % 15 == 0:
		result = "BizzFuzz"

	return result