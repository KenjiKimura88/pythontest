from django.core.urlresolvers import reverse_lazy

from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
import csv

from django.http import HttpResponse
from .models import User

class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('User:list')

class UserList(ListView):
    model = User

class UserDetail(DetailView):
    model = User

class UserUpdate(UpdateView):
    model = User
    success_url = reverse_lazy('User:list')
    fields = ['Username', 'Birthday', 'Random']

class UserCreation(CreateView):
    model = User
    success_url = reverse_lazy('User:list')
    fields = ['Username', 'Birthday']

def export_users_csv(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'

    writer = csv.writer(response)
    writer.writerow(['Username', 'Birthday', 'Random'])

    users = User.objects.all().values_list('Username', 'Birthday', 'Random')
    for user in users:
        writer.writerow(user)

    return response