#Python Test Code
##Set up Environment
- First, update your local package index with apt, and then install the python-django package:
	sudo apt-get update
	sudo apt-get install python-pip

- Install VirtualEnv

	sudo pip install virtualenv

	virtualenv newenv

	source newenv/bin/activate  //Activate Virtual Env

	sudo pip install django==1.9

- You can test that the installation was successful by typing:
	django-admin --version

##Project Creation
- Create Project
	django-admin startproject PythonCodeTask

##Install Django Pip Package
- cd PythonCodeTask
- pip install -r requirements.txt 


##Run server
- python manage.py runserver 0.0.0.0:8000
